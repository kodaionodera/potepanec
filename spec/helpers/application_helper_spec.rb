require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  describe "full_titile method" do
    context "引数が設定されていない場合" do
      it "デフォルトタイトルを返す" do
        expect(full_title).to eq "BIGBAG Store"
      end
    end

    context "引数が設定されている場合" do
      it "引数を合わせたタイトルを返す" do
        expect(full_title("hoge")).to eq "hoge - BIGBAG Store"
      end
    end
  end
end
