require 'rails_helper'

RSpec.feature "Categories", type: :feature do
  given!(:taxonomy) { create(:taxonomy, name: "Category") }
  given!(:taxon1) { create(:taxon, taxonomy: taxonomy, name: "Cloting", parent: taxonomy.root) }
  given!(:taxon2) { create(:taxon, taxonomy: taxonomy, name: "Mugs", parent: taxonomy.root) }
  given!(:taxon_child) { create(:taxon, taxonomy: taxonomy, name: "Shirts", parent: taxon1) }
  given!(:product1) { create(:product, price: 20, taxons: [taxon_child], name: "T-shirts") }
  given!(:product2) { create(:product, price: 30, taxons: [taxon2], name: "Stein") }

  background do
    visit potepan_category_path(taxon_child.id)
  end

  scenario "アクセスするとサイドバーに商品カテゴリーが表示される" do
    expect(page).to have_current_path(potepan_category_path(taxon_child.id))
    expect(page).to have_title(taxon_child.name)
    within('.side-nav') do
      expect(page).to have_content(taxonomy.name)
      expect(page).to have_content(taxon_child.name)
      expect(page).to have_content(taxon2.name)
      expect(page).not_to have_content(taxon1.name)
    end
    expect(page).to have_selector '.productCaption h5', text: product1.name
    expect(page).to have_selector '.productCaption h3', text: product1.display_price
    expect(page).not_to have_selector '.productCaption h5', text: product2.name
    expect(page).not_to have_selector '.productCaption h3', text: product2.display_price
  end

  scenario "カテゴリーをクリックすると、そのカテゴリー商品一覧が表示される" do
    click_link(taxon2.name)
    expect(page).to have_selector '.productCaption h5', text: product2.name
    expect(page).to have_selector '.productCaption h3', text: product2.display_price
    expect(page).not_to have_selector '.productCaption h5', text: product1.name
    expect(page).not_to have_selector '.productCaption h3', text: product1.display_price
  end

  scenario "カテゴリーの商品一覧から商品をクリックすると、その商品詳細ページに移行する" do
    click_link(product1.name)
    expect(page).to have_current_path(potepan_product_path(product1.id))
    expect(page).to have_selector '.media-body h2', text: product1.name
    expect(page).to have_selector '.media-body h3', text: product1.display_price
    expect(page).not_to have_selector '.media-body h2', text: product2.name
    expect(page).not_to have_selector '.media-body h3', text: product2.display_price
  end
end
