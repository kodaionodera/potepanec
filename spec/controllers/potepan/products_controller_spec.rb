require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "#show" do
    let(:product) { create(:product) }
    let(:product_property) { create(:product_property, product: product) }
    # テストで依存するオブジェクトの定義

    before do
      get :show, params: { id: product.id }
    end

    it "レスポンスの成功" do
      expect(response).to be_successful
    end

    it "200レスポンスを返す" do
      expect(response).to have_http_status 200
    end

    it "showテンプレートの表示" do
      expect(response).to render_template :show
    end

    it "商品の表示" do
      expect(assigns(:product)).to eq(product)
    end

    it "商品プロパティの表示" do
      expect(assigns(:product_properties)).to match_array(product_property)
    end
  end
end
