require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "#show" do
    let!(:taxonomy) { create(:taxonomy) }
    let!(:taxon) { create(:taxon, taxonomy: taxonomy, parent: taxonomy.root) }
    let!(:taxon_child) { create(:taxon, taxonomy: taxonomy, parent: taxon) }
    let!(:product1) { create(:product, name: "紐付いた商品", taxons: [taxon_child]) }
    let!(:product2) { create(:product, name: "紐付かない商品") }
    # product2は関連付けさせないことで、紐付かない商品として扱う

    before do
      get :show, params: { id: taxon_child.id }
    end

    it "レスポンスの成功" do
      expect(response).to be_successful
    end

    it "200レスポンスを返す" do
      expect(response).to have_http_status 200
    end

    it "showテンプレートの表示" do
      expect(response).to render_template :show
    end

    it "タクソノミーの表示" do
      expect(assigns(:taxonomies)).to match_array(taxonomy)
    end

    it "タクソンの表示" do
      expect(assigns(:taxon)).to eq(taxon_child)
    end

    it "紐付いた商品の表示" do
      expect(assigns(:products)).to match_array(product1)
    end

    it "紐付かない商品は非表示" do
      expect(assigns(:products)).not_to include(product2)
    end

    it "空idはエラーになる" do
      expect { get :show, params: { id: '' } }.to raise_error(ActiveRecord::RecordNotFound)
    end
  end
end
